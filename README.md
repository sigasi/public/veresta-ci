# Veresta-CI

This project demonstrates the use of Sigasi Veresta for code
validation in a Continuous Integration (CI) environment.

Running code validation in CI helps to maintain and improve code
quality. The earlier code flaws are found, the easier it is to repair
them. And keeping flawed code out of the main branch of your code
repository (e.g. using a *quality gate*) ensures that no new development
is based on known bugs.

This demonstration project is based on the VHDL tutorial from Sigasi Studio.

## How to use this project

### Gitlab CI

We've posted a [technical article on the use of Veresta in Gitlab
CI](https://insights.sigasi.com/tech/veresta-gitlab-ci), check it out
for a more in-depth discussion of the topic!

You need to set up a [Gitlab runner](https://docs.gitlab.com/runner/)
for the CI jobs. You can either set up a *docker* runner, in which
case you also need to prepare a docker container which includes
Veresta. Or you can set up a *shell* or *ssh* runner on a machine with
Veresta installed. Depending on your configuration, it may be useful
to give both your gitlab runner and you gitlab CI job a `veresta`
label to ensure that Veresta jobs run on a node which supports
Veresta.

The Gitlab CI job is configured in `.gitlab-ci.yml` .

Remember to set up your Veresta license. Note that your Gitlab runner
needs to have access to the license server! You can set up a pointer
to the license (either a license file or a license server) using the
`SIGASI_LM_LICENSE_FILE` or `LM_LICENSE_FILE` varaible. These
variables can be set either in `.gitlab-ci.yml` or as CI variables in
Gitlab.

You also need to build the container image for your Veresta runs. The
`Dockerfile` is included in the `docker` folder.  The aforementioned
article discusses the use of a container registry to make the image
available to runners on remote servers.

Once the Gitlab runners, the container image and the Gitlab CI
configuration are in place, you can run the CI pipeline by pushing a
changeset to Gitlab.

### Jenkins

We've posted a [technical article on the use of Veresta in Gitlab
CI](https://insights.sigasi.com/tech/veresta-jenkins), check it out
for a more in-depth discussion of the topic!

You'll need a [Jenkins](https://www.jenkins.io/) server and a build
executor which has Veresta installed.  Install the `warnings-ng`
plugin for reporting Veresta's findings in Jenkins.  If not all of
your build executors have Veresta installed, it's a good idea to use
labels to ensure that your Veresta jobs run on an executor with
Veresta.

You can either set up a freestyle Jenkins project, or use the Jenkins
pipeline definition in the `Jenkinsfile` in this folder. You'll need
to configure Veresta's `PATH` and the license in `Jenkinsfile`. Please
make sure that the build executor can access the license server.

Once your Jenkins project and (if applicable) your `Jenkinsfile` are
in place, you can *build* the project from the Jenkins UI and evaluate
the results.

## Links

[Sigasi website](https://www.sigasi.com)

[Sigasi Veresta product page](https://www.sigasi.com/veresta)

[Sigasi Veresta manual](https://insights.sigasi.com/veresta)

[Tech articles on Sigasi Veresta](https://insights.sigasi.com/tags/veresta)