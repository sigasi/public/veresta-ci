import json
import hashlib

f = open('veresta.json')
data = json.load(f)
f.close()

new_data = []
for issue in data["issues"]:
    print(str(issue))
    new_issue = {'description': issue['description'],
                 'location': {'path': issue['resource'], 'lines': {'begin': issue['line']}},
                 'fingerprint': hashlib.md5(str(issue).encode('utf-8')).hexdigest()}
    if issue['severity'] == 'INFO':
        new_issue['severity'] = 'info'
    elif issue['severity'] == 'WARNING':
        new_issue['severity'] = 'minor'
    elif issue['severity'] == 'ERROR':
        new_issue['severity'] = 'critical'
    else:
        raise ValueError('Severity contains an expected value: ' + str(issue['severity']))
    print('==>' + str(new_issue))
    new_data.append(new_issue)

with open('veresta_gitlab.json', 'w') as outfile:
    json.dump(new_data, outfile)
