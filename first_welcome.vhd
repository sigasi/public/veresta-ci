library ieee; use ieee.std_logic_1164.all;

entity welcome is     port(
clk:in std_logic; -- This is the main clock
rst:in std_logic;
data_in:in std_logic;
data_out:out std_logic
);end entity welcome;

architecture RTL of welcome is
type mytype is (a, b, c);
                signal state : mytype;
begin
	
-- the main process of this architecture
name : process(clk) is begin

if (rst  == '1') then
			
elsif rising_edge(clk) then
	
data_out := data_in;
end if;
end process name;

end architecture RTL;
