library my_lib;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity foo is
end entity foo;

architecture STR of foo is
	signal clk : std_logic;
begin
	clock_inst : entity my_lib.clock_generator(BEH)
		generic map(PERIOD => 20 ns)
		port map(clk => clk);
end architecture STR;
